import Phaser from "phaser";

import MainScene from "./scenes/MainScene";

const width = 1920 * window.devicePixelRatio;
const height = 1080 * window.devicePixelRatio;

const config = {
    type: Phaser.AUTO,
    backgroundColor: "#FFFFFF",
    width,
    height,
    physics: {
        default: "arcade",
        arcade: {},
    },
    scene: [MainScene],
    globalVars: {},
    scale: {
        parent: "gameBody",
        mode: Phaser.Scale.ScaleModes.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        zoom: 1 / window.devicePixelRatio,
        width: width * window.devicePixelRatio,
        height: height * window.devicePixelRatio,
    },
};

const game = new Phaser.Game(config);

export default game;
