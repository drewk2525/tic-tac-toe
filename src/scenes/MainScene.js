import Phaser from "phaser";
const rng = new Phaser.Math.RandomDataGenerator();

const X_COLOR = 0xc459e4;
const O_COLOR = 0x65a8ff;
const BIOME_REWARDS = {
    desert: "coin",
    forest: "wood",
    mountains: "ore",
    plains: "hide",
};

export default class MainScene extends Phaser.Scene {
    constructor() {
        super("main-scene");
    }

    preload() {
        // Resource Icons
        this.load.image("coin", "coin.png");
        this.load.image("hide", "hide.png");
        this.load.image("ore", "ore.png");
        this.load.image("wood", "wood.png");

        // Tile Icons
        this.load.image("desert", "tiles/desert.png");
        this.load.image("forest", "tiles/forest.png");
        this.load.image("mountains", "tiles/mountains.png");
        this.load.image("plains", "tiles/plains.png");

        // Cards
        this.load.image("pass", "cards/pass.png");
        this.load.image("haste", "cards/haste.png");
        this.load.image("random", "cards/random.png");
        this.load.image("remove", "cards/remove.png");

        // Other
        this.load.image("conch", "conch.png");
        this.load.image("x", "x.png");
    }

    create() {
        const iconSpacing = 100;
        const is = 100;
        const iconYOffset = 100;
        const player2YPosition = (this.scale.height + iconYOffset) / 2;
        const p2y = player2YPosition + 70;
        const textConfig = {
            fontFamily: "sans-serif",
            fontSize: "25px",
            color: "#000000",
        };
        this.playerResources = {
            x: {
                coin: 0,
                hide: 0,
                ore: 0,
                wood: 0,
                coinText: this.add.text(30, 170, "0", textConfig),
                hideText: this.add.text(iconSpacing + 30, 170, "0", textConfig),
                oreText: this.add.text(is * 2 + 30, 170, "0", textConfig),
                woodText: this.add.text(is * 3 + 30, 170, "0", textConfig),
            },
            o: {
                coin: 0,
                hide: 0,
                ore: 0,
                wood: 0,
                coinText: this.add.text(30, p2y, "0", textConfig),
                hideText: this.add.text(is + 30, p2y, "0", textConfig),
                oreText: this.add.text(is * 2 + 30, p2y, "0", textConfig),
                woodText: this.add.text(is * 3 + 30, p2y, "0", textConfig),
            },
        };
        this.currentTurn = "x";
        this.phase = "cell";
        this.gameOver = false;
        this.graphics = this.add.graphics();
        const gridSpacing = 360;
        const resIcons = [];
        this.conchLocations = { x: 300, y1: 0, y2: player2YPosition - 100 };
        this.conch = this.add.image(300, 0, "conch");
        this.conch.setScale(0.3);
        this.conch.setOrigin(0);

        this.consoleText = this.add.text(0, 350, "X Player Move", {
            fontFamily: "sans-serif",
            fontSize: "25px",
            color: "#000000",
        });

        this.placeCards();

        // Player 1 resource icons
        this.drawX(10, 10, 0.6);
        this.add.text(100, 0, "Player", {
            fontFamily: "sans-serif",
            fontSize: "60px",
            color: "#000000",
            fontStyle: "bold",
        });
        resIcons.push(this.add.image(0, iconYOffset, "coin"));
        resIcons.push(this.add.image(iconSpacing, iconYOffset, "hide"));
        resIcons.push(this.add.image(iconSpacing * 2, iconYOffset, "ore"));
        resIcons.push(this.add.image(iconSpacing * 3, iconYOffset, "wood"));

        // Player 2 resource icons
        this.drawO(10, player2YPosition - 90, 30);
        this.add.text(100, player2YPosition - 100, "Player", {
            fontFamily: "sans-serif",
            fontSize: "60px",
            color: "#000000",
            fontStyle: "bold",
        });
        resIcons.push(this.add.image(0, player2YPosition, "coin"));
        resIcons.push(this.add.image(iconSpacing, player2YPosition, "hide"));
        resIcons.push(this.add.image(iconSpacing * 2, player2YPosition, "ore"));
        resIcons.push(
            this.add.image(iconSpacing * 3, player2YPosition, "wood")
        );

        // Scale resource icons
        resIcons.forEach((icon) => {
            icon.setScale(0.15);
            icon.setOrigin(0);
        });

        // Draw Main Grid
        const gridxOffset = (this.scale.width - gridSpacing * 3) / 2;
        this.drawBoard(gridSpacing, gridxOffset, 0, 10);

        // Draw Sub-grids
        this.gridmap = [];
        let currentGrid = [];
        for (let x = 0; x < 9; x++) {
            currentGrid = [];
            for (let i = 0; i < 3; i++) {
                const row = [];
                for (let j = 0; j < 3; j++) {
                    row.push({ biome: this.getRandomTile() });
                }
                currentGrid.push(row);
            }
            this.gridmap.push({
                index: x,
                grid: currentGrid,
                playerVictory: "none",
            });
        }
        this.gridmap.forEach(({ grid }, gridIndex) => {
            // Draw Grid
            const subGridSpacing = gridSpacing / 3;
            const gridX = gridIndex % 3;
            const gridY = Math.floor(gridIndex / 3);
            const xOffset = gridxOffset + gridSpacing * gridX;
            const yOffset = gridSpacing * gridY;
            this.drawBoard(subGridSpacing, xOffset, yOffset);

            grid.forEach((row, rowIndex) => {
                row.forEach((cell, cellIndex) => {
                    // Display Cell Image
                    const padding = 10;
                    const imageX =
                        xOffset + subGridSpacing * rowIndex + padding;
                    const imageY =
                        yOffset + subGridSpacing * cellIndex + padding;
                    const cellImage = this.add.image(
                        imageX,
                        imageY,
                        cell.biome
                    );
                    cell.sprite = cellImage;
                    cellImage.setScale(0.1);
                    cellImage.setOrigin(0);
                    cellImage.setInteractive();
                    cellImage.on("pointerdown", () => {
                        const playerVictory =
                            this.gridmap[gridIndex].playerVictory;
                        if (
                            playerVictory == "none" &&
                            cell.occupied &&
                            this.phase == "remove"
                        ) {
                            cell.token.destroy();
                            cell.occupied = null;
                            this.switchTurn();
                            return;
                        }
                        if (
                            cell.occupied ||
                            playerVictory != "none" ||
                            this.gameOver ||
                            this.phase != "cell"
                        ) {
                            return;
                        }
                        this.playerResources[this.currentTurn][
                            BIOME_REWARDS[cell.biome]
                        ]++;
                        const resources =
                            this.playerResources[this.currentTurn][
                                BIOME_REWARDS[cell.biome]
                            ];
                        this.playerResources[this.currentTurn][
                            `${BIOME_REWARDS[cell.biome]}Text`
                        ].text = resources.toString();
                        this.graphics.setDepth(10);
                        cell.occupied = this.currentTurn;
                        const { xVictory, oVictory } =
                            this.checkGridVictory(grid);
                        if (xVictory) {
                            this.drawX(xOffset, yOffset, 3.5);
                            this.disableGrid(this.gridmap[gridIndex], "x");
                            this.checkGameVictory(this.gridmap);
                        }
                        if (oVictory) {
                            this.drawO(
                                xOffset + 5,
                                yOffset + 5,
                                gridSpacing / 2 - 10,
                                20
                            );
                            this.disableGrid(this.gridmap[gridIndex], "o");
                            this.checkGameVictory(this.gridmap);
                        }
                        if (this.currentTurn === "x") {
                            cell.token = this.drawX(imageX, imageY);
                            this.consoleText.text = "X Player Select Card";
                        } else {
                            cell.token = this.drawO(imageX, imageY);
                            this.consoleText.text = "Y Player Select Card";
                        }
                        this.phase = "card";
                    });
                });
            });
        });
    }

    checkGameVictory(gridmap) {
        const { xVictory, oVictory } = this.checkGridVictory(gridmap);
        if (xVictory) {
            this.gameOver = true;
            this.consoleText.text = "X WINS!!!";
        }
        if (oVictory) {
            this.gameOver = true;
            this.consoleText.text = "O WINS!!!";
        }
    }

    disableGrid(gridmap, playerVictory) {
        const tint = playerVictory === "x" ? X_COLOR : O_COLOR;
        gridmap.playerVictory = playerVictory;
        gridmap.grid.forEach((row) => {
            row.forEach((cell) => {
                cell.sprite.setTint(tint);
            });
        });
    }

    checkGridVictory(grid) {
        const xOccupied = this.getPlayerOccupiedSpaces(grid, "x");
        const oOccupied = this.getPlayerOccupiedSpaces(grid, "o");
        const xVictory = this.playerVictory(xOccupied);
        const oVictory = this.playerVictory(oOccupied);
        return { xVictory, oVictory };
    }

    playerVictory(occupied) {
        let victory = false;
        // Row victory
        victory = this.checkVictoryCondition(occupied, [0, 1, 2]) || victory;
        victory = this.checkVictoryCondition(occupied, [3, 4, 5]) || victory;
        victory = this.checkVictoryCondition(occupied, [6, 7, 8]) || victory;
        // Column victory
        victory = this.checkVictoryCondition(occupied, [0, 3, 6]) || victory;
        victory = this.checkVictoryCondition(occupied, [1, 4, 7]) || victory;
        victory = this.checkVictoryCondition(occupied, [2, 5, 8]) || victory;
        // Diagonal victory
        victory = this.checkVictoryCondition(occupied, [0, 4, 8]) || victory;
        victory = this.checkVictoryCondition(occupied, [2, 4, 6]) || victory;
        return victory;
    }

    checkVictoryCondition(occupied, conditions) {
        return (
            occupied.includes(conditions[0]) &&
            occupied.includes(conditions[1]) &&
            occupied.includes(conditions[2])
        );
    }

    getPlayerOccupiedSpaces(grid, player) {
        return grid.reduce((rowAcc, row, i) => {
            let rowResults = [];
            if (row.playerVictory === player) {
                rowAcc.push(i);
            }
            if (!row.playerVictory) {
                rowResults = row.reduce((cellAcc, cell, j) => {
                    if (cell.occupied === player) {
                        cellAcc.push(j * 3 + i);
                    }
                    return cellAcc;
                }, []);
            }
            return [...rowAcc, ...rowResults];
        }, []);
    }

    getRandomTile() {
        const tiles = [
            "desert",
            "forest",
            "forest",
            "forest",
            "mountains",
            "mountains",
            "plains",
            "plains",
            "plains",
        ];
        return tiles[rng.between(0, tiles.length - 1)];
    }

    drawBoard(spacing, xOffset, yOffset, lineWidth = 2) {
        const lineColor = lineWidth === 2 ? 0x000000 : 0x990000;
        const graphics = this.graphics;
        graphics.lineStyle(lineWidth, lineColor);
        const line1 = new Phaser.Geom.Line(
            spacing + xOffset,
            yOffset,
            spacing + xOffset,
            spacing * 3 + yOffset
        );
        graphics.strokeLineShape(line1);
        const line2 = new Phaser.Geom.Line(
            spacing * 2 + xOffset,
            yOffset,
            spacing * 2 + xOffset,
            spacing * 3 + yOffset
        );
        graphics.strokeLineShape(line2);
        const line3 = new Phaser.Geom.Line(
            xOffset,
            spacing + yOffset,
            spacing * 3 + xOffset,
            spacing + yOffset
        );
        graphics.strokeLineShape(line3);
        const line4 = new Phaser.Geom.Line(
            xOffset,
            spacing * 2 + yOffset,
            spacing * 3 + xOffset,
            spacing * 2 + yOffset
        );
        graphics.strokeLineShape(line4);
    }

    drawX(x, y, scale = 1) {
        const image = this.add.image(x, y, "x");
        image.setOrigin(0);
        image.setScale(scale);
        return image;
    }

    drawO(x, y, radius = 50, width = 10) {
        const circle = this.add.circle(x + radius, y + radius, radius);
        circle.setStrokeStyle(width, O_COLOR);
        return circle;
    }

    switchTurn() {
        if (this.gameOver) {
            return;
        }
        if (this.currentTurn === "x") {
            this.currentTurn = "o";
            this.add.tween({
                targets: this.conch,
                x: this.conchLocations.x,
                y: this.conchLocations.y2,
                duration: 500,
            });
            this.consoleText.text = "O Player Move";
        } else {
            this.currentTurn = "x";
            this.add.tween({
                targets: this.conch,
                x: this.conchLocations.x,
                y: this.conchLocations.y1,
                duration: 500,
            });
            this.consoleText.text = "X Player Move";
        }
        this.phase = "cell";
    }

    updatePlayerResources(resource, qty, player) {
        this.playerResources[player][resource] -= qty;
        this.playerResources[player][`${resource}Text`].text =
            this.playerResources[player][resource].toString();
    }

    placeCards() {
        const cards = [];
        const cardAbilities = [];
        const x = 1510;
        const xOffset = 210;
        const yOffset = 300;
        /**
         * Pass
         */
        cards.push(this.add.image(x, 0, "pass"));
        cardAbilities.push(() => {
            this.switchTurn();
        });
        /**
         * Haste
         */
        cards.push(this.add.image(x + xOffset, 0, "haste"));
        cardAbilities.push(() => {
            const resources = this.playerResources[this.currentTurn];
            if (
                resources.coin >= 1 &&
                resources.wood >= 2 &&
                resources.ore >= 1
            ) {
                this.updatePlayerResources("coin", 1, this.currentTurn);
                this.updatePlayerResources("wood", 2, this.currentTurn);
                this.updatePlayerResources("ore", 1, this.currentTurn);
                this.phase = "cell";
                this.consoleText.text = `${this.currentTurn.toUpperCase()} Player Move`;
            }
        });
        /**
         * Random
         */
        cards.push(this.add.image(x, yOffset, "random"));
        cardAbilities.push(() => {
            const resources = this.playerResources[this.currentTurn];
            if (resources.wood >= 2 && resources.hide >= 1) {
                this.updatePlayerResources("wood", 2, this.currentTurn);
                this.updatePlayerResources("hide", 1, this.currentTurn);
                const availableCells = this.gridmap.reduce(
                    (acc, grid, gridIndex) => {
                        grid.grid.forEach((row, rowIndex) => {
                            row.forEach((cell, cellIndex) => {
                                if (
                                    !cell.occupied &&
                                    grid.playerVictory == "none"
                                ) {
                                    acc.push({
                                        gridIndex,
                                        rowIndex,
                                        cellIndex,
                                    });
                                }
                            });
                        });
                        return acc;
                    },
                    []
                );
                const cell =
                    availableCells[rng.between(0, availableCells.length - 1)];
                console.log({ cell });
                const x =
                    this.gridmap[cell.gridIndex].grid[cell.rowIndex][
                        cell.cellIndex
                    ].sprite.x;
                const y =
                    this.gridmap[cell.gridIndex].grid[cell.rowIndex][
                        cell.cellIndex
                    ].sprite.y;
                const resource =
                    BIOME_REWARDS[
                        this.gridmap[cell.gridIndex].grid[cell.rowIndex][
                            cell.cellIndex
                        ].biome
                    ];
                this.playerResources[this.currentTurn][resource] += 1;
                if (this.currentTurn === "x") {
                    this.drawX(x, y);
                } else {
                    this.drawO(x, y);
                }
                this.gridmap[cell.gridIndex].grid[cell.rowIndex][
                    cell.cellIndex
                ].occupied = this.currentTurn;
                this.switchTurn();
            }
        });
        /**
         * Remove
         */
        cards.push(this.add.image(x + xOffset, yOffset, "remove"));
        cardAbilities.push(() => {
            const resources = this.playerResources[this.currentTurn];
            if (resources.hide >= 3 && resources.ore >= 2) {
                this.updatePlayerResources("hide", 3, this.currentTurn);
                this.updatePlayerResources("ore", 2, this.currentTurn);
                this.phase = "remove";
                this.consoleText.text = `${this.currentTurn.toUpperCase()} Player - Remove a token`;
            }
        });

        cards.forEach((card, i) => {
            card.setOrigin(0);
            card.setScale(0.14);
            card.setInteractive();
            card.on("pointerdown", () => {
                if (this.phase != "card") {
                    return;
                }
                cardAbilities[i]();
            });
        });
    }
}
